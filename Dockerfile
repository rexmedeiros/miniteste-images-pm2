FROM php:7.4-cli

RUN apt-get update && apt-get install -y apt-utils 

RUN curl -sL https://deb.nodesource.com/setup_lts.x  | bash -

RUN apt-get install -y nodejs

RUN npm install pm2 -g

WORKDIR /src

EXPOSE 6379 1367 1368
